from django import forms
from pigenUwU.models import Portofolio

class Portofolio_Form(forms.ModelForm):
    class Meta:
            model = Portofolio
            fields = ("projecttitle","startdate","enddate","description","place","category")
            labels = {
                "projecttitle":"Project Title",
                "startdate":"Start Date",
                "enddate":"End Date",
                "description":"Description",
                "place":"Place",
                "category":"Category"
            }