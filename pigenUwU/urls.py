from django.contrib import admin
from django.urls import path
from pigenUwU import views
from django.conf.urls import url


urlpatterns = [
    path('',views.about,name='home'),
    path('Home',views.home,name='home'),
    path('Profile', views.about,name="about"),
    path('More',views.more,name="more"),
    path('Sign Up',views.signup,name="signup"),
    path('Form',views.form,name="form"),
    path('Project',views.project,name="project")
]