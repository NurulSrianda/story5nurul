from django.shortcuts import render, redirect
from datetime import datetime
from pigenUwU.models import Portofolio
from pigenUwU.forms import Portofolio_Form

# Create your views here.
waktu = datetime.now()

def home(request):
    return render(request,'pages/Story3.html',{'time':waktu})

def about(request):
    return render(request,'pages/Story3.html',{'time':waktu})

def more(request):
    return render(request,'pages/baru.html',{'time':waktu})

def signup(request):
    return render(request,'pages/sign-up.html',{'time':waktu})

def project(request):
    databaru = Portofolio.objects.all() # Ambil data dari database
    return render(request,'pages/project.html',{"data":databaru})

def form(request):
    if request.method == "POST":
        data = Portofolio_Form(request.POST)
        if data.is_valid():
            data.save() # save ke database
        else:
           print(data.errors)
    return render(request,'pages/form-porto.html',{"form":Portofolio_Form()})
    return redirect("/pages/project.html")