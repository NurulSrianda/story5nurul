from django.db import models

# Create your models here.

class Portofolio(models.Model):
    projecttitle = models.CharField(max_length = 150)
    startdate = models.DateField()
    enddate = models.DateField()
    description = models.TextField()
    place = models.CharField(max_length = 100)
    category = models.CharField(max_length = 50)